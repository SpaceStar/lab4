package lab4;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Locale;

@Stateless
public class UserBean {
    private EntityManagerFactory entityManagerFactory;

    public UserBean() {
        Locale.setDefault(Locale.ENGLISH);
        entityManagerFactory = Persistence.createEntityManagerFactory("lab4.database");
    }

    public List<UsersInfoEntity> loadUser(String login) {
        List<UsersInfoEntity> user;
        EntityManager em = entityManagerFactory.createEntityManager();
        user = em.createQuery("select u from UsersInfoEntity u WHERE u.login like :login", UsersInfoEntity.class)
                .setParameter("login", login).getResultList();
        em.close();
        return user;
    }
}
