package lab4;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.List;

@Stateless
@Path("/point")
public class ApiBean {
    @EJB
    private PointBean point;

    @EJB
    private HistoryBean historyBean;

    @EJB
    private UserBean userBean;

    public ApiBean() {
    }

    @HEAD
    @Path("/x/{value}")
    public Response setX(@PathParam("value") double value) {
        point.setX(value);
        return Response.ok().header("Access-Control-Allow-Origin", "*").build();
    }

    @HEAD
    @Path("/y/{value}")
    public Response setY(@PathParam("value") double value) {
        point.setY(value);
        return Response.ok().header("Access-Control-Allow-Origin", "*").build();
    }

    @HEAD
    @Path("/r/{value}")
    public Response setR(@PathParam("value") double value) {
        point.setR(value);
        return Response.ok().header("Access-Control-Allow-Origin", "*").build();
    }

    @GET
    @Path("/result")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getResult() {
        point.checkResult();
        historyBean.addPoint(new PointEntity(point));
        return Response.ok(String.valueOf(point.isResult())).header("Access-Control-Allow-Origin", "*").build();
    }

    @GET
    @Path("/current")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCurrent() {
        JsonObject rootObject = new JsonObject();
        rootObject.addProperty("x", point.getX());
        rootObject.addProperty("y", point.getY());
        rootObject.addProperty("r", point.getR());
        Gson gson = new Gson();
        String json = gson.toJson(rootObject);
        return Response.ok(json).header("Access-Control-Allow-Origin", "*").build();
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        Gson gson = new Gson();
        String json = gson.toJson(historyBean.getPoints());
        return Response.ok(json).header("Access-Control-Allow-Origin", "*").build();
    }

    @HEAD
    @Path("/clear")
    public Response clearHistory() {
        historyBean.clearPoints();
        return Response.ok().header("Access-Control-Allow-Origin", "*").build();
    }

    @GET
    @Path("/check/{user}/{pass}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response checkPass(@PathParam("user") String login, @PathParam("pass") String pass) throws Exception {
        List<UsersInfoEntity> users = userBean.loadUser(login);
        if (users.size() == 0)
            return Response.ok("false").header("Access-Control-Allow-Origin", "*").build();
        UsersInfoEntity user = users.get(0);
        MessageDigest digest = MessageDigest.getInstance("SHA-256"); // try-catch or throwable method
        byte[] hash = digest.digest(pass.getBytes(StandardCharsets.UTF_8)); // get entered pass in sha-256

        String hashEnteredPass = bytesToHex(hash);
        boolean result = hashEnteredPass.equals(user.getShaPass());

        return Response.ok(String.valueOf(result)).header("Access-Control-Allow-Origin", "*").build();
    }

    private String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
