package lab4;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LogPas")
public class UsersInfoEntity {

    @Id
    private String login;

    @Column(nullable = false)
    private String shaPass;

    public UsersInfoEntity() {}

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getShaPass() {
        return shaPass;
    }

    public void setShaPass(String shaPass) {
        this.shaPass = shaPass;
    }
}
