package lab4;

import javax.ejb.Stateless;

@Stateless
public class PointBean {
    private double x;
    private double y;
    private double r = 1;
    private boolean result = true;

    public PointBean() {}

    public void checkResult() {
        this.result = Checker.check(x, y, r);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public boolean isResult() {
        return result;
    }

    public String getResultAsString() {
        if (result)
            return "inside";
        return "outside";
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
