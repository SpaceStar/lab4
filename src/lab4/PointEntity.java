package lab4;

import javax.persistence.*;

@Entity
@Table(name = "points")
public class PointEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "point_id_seq")
    @SequenceGenerator(name = "point_id_seq", sequenceName = "point_id_seq", allocationSize = 1)
    private long id;

    @Column(nullable = false)
    private double x;

    @Column(nullable = false)
    private double y;

    @Column(nullable = false)
    private double r = 1;

    @Column(nullable = false)
    private boolean result = true;

    public PointEntity() {}

    public PointEntity(PointBean point) {
        this.x = point.getX();
        this.y = point.getY();
        this.r = point.getR();
        this.result = point.isResult();
    }

    public void checkResult() {
        this.result = Checker.check(x, y, r);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public boolean isResult() {
        return result;
    }

    public String getResultAsString() {
        if (result)
            return "inside";
        return "outside";
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
