package lab4;

class Checker {
    private Checker() {}

    public static boolean check(double x, double y, double r) {

        return (((x * x + y * y <= r * r / 4) && (x <= 0) && (y >= 0)) ||
                ((x >= 0) && (y >= 0) && (y <= r - x)) ||
                ((x >= 0) && (x <= r) && (y <= 0) && (y >= -r / 2)));


    }
}
