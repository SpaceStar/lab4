package lab4;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Locale;

@Stateless
public class HistoryBean {
    private EntityManagerFactory entityManagerFactory;

    public HistoryBean() {
        Locale.setDefault(Locale.ENGLISH);
        entityManagerFactory = Persistence.createEntityManagerFactory("lab4.database");
    }

    public void addPoint(PointEntity pointEntity) {
        savePoint(pointEntity);
    }

    public void clearPoints() {
        clearTable();
    }

    public List<PointEntity> getPoints() {
        return loadPoints();
    }

    private void savePoint(PointEntity pointEntity) {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.persist(pointEntity);
        em.getTransaction().commit();
        em.close();
    }

    private List<PointEntity> loadPoints() {
        List<PointEntity> points;
        EntityManager em = entityManagerFactory.createEntityManager();
        points = em.createQuery("from PointEntity", PointEntity.class).getResultList();
        em.close();
        return points;
    }

    private void clearTable() {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.createQuery("DELETE FROM PointEntity").executeUpdate();
        em.getTransaction().commit();
        em.close();
    }
}
