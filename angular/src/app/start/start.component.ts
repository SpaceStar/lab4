import {AfterViewInit, Component, OnInit} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {MainComponent} from '../main/main.component';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css']
})
export class StartComponent implements OnInit, AfterViewInit {

  username: string;
  password: string;

  constructor(private http: HttpClient, private cookieService: CookieService) { }

  static error1TurnOn() {
    document.getElementById('error1-msg').style.visibility = 'visible';
  }

  static error1TurnOff() {
    document.getElementById('error1-msg').style.visibility = 'hidden';
  }

  static error2TurnOn() {
    document.getElementById('error2-msg').style.visibility = 'visible';
  }

  static error2TurnOff() {
    document.getElementById('error2-msg').style.visibility = 'hidden';
  }

  ngOnInit() {
    this.redirectIfAuth();
  }

  ngAfterViewInit() {
    StartComponent.error1TurnOff();
    StartComponent.error2TurnOff();
  }

  redirectIfAuth() {
    if (this.cookieService.get('auth') === 'true') {
      window.location.href = '/main';
    }
  }

  loginClick() {
    const url = MainComponent.domain.concat(MainComponent.baseurl, 'check/', this.username, '/', this.password);
    this.http.get(url, {
      responseType: 'text'
    }).subscribe(result => {
      StartComponent.error1TurnOff();
      if (result === 'true') {
        StartComponent.error2TurnOff();
        this.cookieService.set('auth', 'true');
        window.location.href = '/main';
      } else {
        StartComponent.error2TurnOn();
      }
    }, () => {
      StartComponent.error1TurnOn();
    });
  }

}
