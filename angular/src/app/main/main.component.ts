import {AfterViewInit, Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, AfterViewInit {

  static domain = 'http://localhost:8080/lab4_war_exploded/';
  static baseurl = 'api/point/';

  x = 0;
  y = 0;
  r = 1;

  constructor( private http: HttpClient, private cookieService: CookieService ) {}

  static loadingTurnOn() {
    document.getElementById('loading').style.visibility = 'visible';
  }

  static loadingTurnOff() {
    document.getElementById('loading').style.visibility = 'hidden';
  }

  static errorTurnOn() {
    document.getElementById('error-msg').style.visibility = 'visible';
  }

  static errorTurnOff() {
    document.getElementById('error-msg').style.visibility = 'hidden';
  }

  static draw() {
    const graph = <HTMLCanvasElement> document.getElementById('graph');
    const context = graph.getContext('2d');
    context.beginPath();
    context.rect(0, 0, 300, 300);
    context.fillStyle = '#ffffff';
    context.fill();
    context.beginPath();
    context.rect(150, 150, 100, 50);
    context.fillStyle = '#339bff';
    context.fill();
    context.beginPath();
    context.moveTo(150, 150);
    context.lineTo(150, 50);
    context.lineTo(250, 150);
    context.lineTo(150, 150);
    context.fill();
    context.beginPath();
    context.arc(150, 150, 50, Math.PI, 3 * Math.PI / 2);
    context.lineTo(150, 150);
    context.lineTo(100, 150);
    context.fill();
    context.beginPath();
    context.moveTo(150, 300);
    context.lineTo(150, 0);
    context.lineTo(145, 5);
    context.moveTo(150, 0);
    context.lineTo(155, 5);
    context.moveTo(0, 150);
    context.lineTo(300, 150);
    context.lineTo(295, 145);
    context.moveTo(300, 150);
    context.lineTo(295, 155);
    context.moveTo(50, 145);
    context.lineTo(50, 155);
    context.moveTo(100, 145);
    context.lineTo(100, 155);
    context.moveTo(200, 145);
    context.lineTo(200, 155);
    context.moveTo(250, 145);
    context.lineTo(250, 155);
    context.moveTo(145, 50);
    context.lineTo(155, 50);
    context.moveTo(145, 100);
    context.lineTo(155, 100);
    context.moveTo(145, 200);
    context.lineTo(155, 200);
    context.moveTo(145, 250);
    context.lineTo(155, 250);
    context.strokeStyle = '#000000';
    context.stroke();
    context.fillStyle = '#000000';
    context.textAlign = 'right';
    context.textBaseline = 'bottom';
    context.fillText('x', 300, 145);
    context.textAlign = 'left';
    context.textBaseline = 'top';
    context.fillText('y', 160, 0);
    context.textAlign = 'center';
    context.textBaseline = 'bottom';
    context.fillText('-R', 50, 145);
    context.fillText('-R/2', 100, 145);
    context.fillText('R/2', 200, 145);
    context.fillText('R', 250, 145);
    context.textAlign = 'left';
    context.textBaseline = 'middle';
    context.fillText('R', 160, 50);
    context.fillText('R/2', 160, 100);
    context.fillText('-R/2', 160, 200);
    context.fillText('-R', 160, 250);
  }

  static drawDot(x, y, R, result) {
    const graph = <HTMLCanvasElement> document.getElementById('graph');
    const context = graph.getContext('2d');
    x = 150 + (100 * x / R);
    y = 150 - (100 * y / R);
    context.beginPath();
    context.arc(x, y, 5, 0, 2 * Math.PI);
    if (result === 'true') {
      context.fillStyle = '#00ff00';
    } else {
      context.fillStyle = '#ff0000';
    }
    context.fill();
  }

  ngOnInit() {
    this.redirectIfNotAuth();
  }

  redirectIfNotAuth() {
    if (!(this.cookieService.get('auth') === 'true')) {
      window.location.href = '/start';
    }
  }

  ngAfterViewInit() {
    MainComponent.loadingTurnOff();
    MainComponent.errorTurnOff();
    MainComponent.draw();
    this.getParameters();
  }

  getParameters() {
    MainComponent.loadingTurnOn();
    const url = MainComponent.domain.concat(MainComponent.baseurl, 'current');
    this.http.get(url).subscribe(values => {
      this.x = values['x'];
      this.y = values['y'];
      this.r = values['r'];
      MainComponent.loadingTurnOff();
    });
  }

  setX() {
    MainComponent.loadingTurnOn();
    const url = MainComponent.domain.concat(MainComponent.baseurl, 'x/', this.x.toString());
    this.http.head(url).subscribe(() => {
      MainComponent.errorTurnOff();
      MainComponent.loadingTurnOff();
    }, () => {
      MainComponent.errorTurnOn();
      MainComponent.loadingTurnOff();
    });
  }

  setY() {
    MainComponent.loadingTurnOn();
    const url = MainComponent.domain.concat(MainComponent.baseurl, 'y/', this.y.toString());
    this.http.head(url).subscribe(() => {
      MainComponent.errorTurnOff();
      MainComponent.loadingTurnOff();
    }, () => {
      MainComponent.errorTurnOn();
      MainComponent.loadingTurnOff();
    });
  }

  setR() {
    MainComponent.loadingTurnOn();
    const url = MainComponent.domain.concat(MainComponent.baseurl, 'r/', this.r.toString());
    this.http.head(url).subscribe(() => {
      MainComponent.errorTurnOff();
      MainComponent.loadingTurnOff();
    }, () => {
      MainComponent.errorTurnOn();
      MainComponent.loadingTurnOff();
    });
    this.check();
  }

  check() {
    MainComponent.loadingTurnOn();
    const url = MainComponent.domain.concat(MainComponent.baseurl, 'result');
    this.http.get(url, {
      responseType: 'text'
    }).subscribe(result => {
      MainComponent.errorTurnOff();
      MainComponent.drawDot(this.x, this.y, this.r, result);
      MainComponent.loadingTurnOff();
    }, () => {
      MainComponent.errorTurnOn();
      MainComponent.loadingTurnOff();
    });
  }

  historyClick() {
    window.location.href = '/history';
  }

  logoutClick() {
    this.cookieService.delete('auth');
    window.location.href = '/start';
  }

  graphClick(event) {
    const graph = document.getElementById('graph');
    const rect = graph.getBoundingClientRect();
    let x = event.clientX - rect.left;
    let y = event.clientY - rect.top;
    x = (x - 150) * this.r / 100;
    y = (150 - y) * this.r / 100;
    this.x = x;
    this.y = y;
    this.setX();
    this.setY();
    this.check();
  }

}
