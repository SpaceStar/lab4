import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Point} from './Point';
import {HttpClient} from '@angular/common/http';
import {MainComponent} from '../main/main.component';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit, AfterViewInit {

  points: Point[];

  constructor(private http: HttpClient, private cookieService: CookieService) { }

  static errorTurnOn() {
    document.getElementById('error-msg').style.visibility = 'visible';
  }

  static errorTurnOff() {
    document.getElementById('error-msg').style.visibility = 'hidden';
  }

  ngOnInit() {
    this.redirectIfNotAuth();
    this.updateData();
  }

  redirectIfNotAuth() {
    if (!(this.cookieService.get('auth') === 'true')) {
      window.location.href = '/start';
    }
  }

  ngAfterViewInit() {
    HistoryComponent.errorTurnOff();
  }

  updateData() {
    const url = MainComponent.domain.concat(MainComponent.baseurl, 'all');
    this.http.get(url).subscribe(value => {
      HistoryComponent.errorTurnOff();
      this.points = <Point[]> value;
    }, () => {
      HistoryComponent.errorTurnOn();
    });
  }

  clearClick() {
    const url = MainComponent.domain.concat(MainComponent.baseurl, 'clear');
    this.http.head(url).subscribe(() => {
      HistoryComponent.errorTurnOff();
      this.updateData();
    }, () => {
      HistoryComponent.errorTurnOn();
    });
  }

  backClick() {
    window.location.href = '/main';
  }

}
