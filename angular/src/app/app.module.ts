import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {StartComponent} from './start/start.component';
import {MainComponent} from './main/main.component';
import {PanelModule} from 'primeng/panel';
import {ButtonModule} from 'primeng/button';
import {InputTextModule, KeyFilterModule, RadioButtonModule} from 'primeng/primeng';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {HistoryComponent} from './history/history.component';
import {TableModule} from 'primeng/table';
import {CookieService} from 'ngx-cookie-service';


@NgModule({
  declarations: [
    AppComponent,
    StartComponent,
    MainComponent,
    HistoryComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    PanelModule,
    ButtonModule,
    RadioButtonModule,
    HttpClientModule,
    FormsModule,
    KeyFilterModule,
    TableModule,
    InputTextModule
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
